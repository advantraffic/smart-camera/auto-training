# Object Detection Auto Training

This is a repository for automatically train object detection models using TensorFlow Object Detection API. Training Image do following steps:

1. Convert images and xml labels to TF record for training.
2. Start training from pretrained model.
3. Freeze TensorFlow training checkpoint to frozen model
4. Export TesnorFlow frozen model to OpenVINO IR.

 ## How to start?

1. Pull training image to your local training PC (if not exist)

```
docker pull ibrahim97/master_auto_training:latest
```

2. Clone current repository

```
 git clone https://gitlab.com/advantraffic/smart-camera/auto-training.gits
```

3. **Copy images to auto-training/dataset/JPEGImages**
4. **Copy labels to auto-training/dataset/Annotations**
5. open terminal and change directory to auto-training/dataset
6. run below command to generate train.txt test.txt (spilt data)

```
python split_data_train_test.py --dataset .
```

7. download [pretrained model from google drive](https://drive.google.com/file/d/1LmtyGjEoEFn7d5YQUz9FutPjhccLCIVX/view?usp=sharing) and unzip it at auto-training/training/pretrained
8. start training using docker (mount drive between host and container + expose TensorBoard port 6006 to 5001)

```
docker run --gpus all -v /path/auto-training:/global -p 5001:6006 ibrahim97/master_auto_training:latest
```

#### Docker Image optional environment variables

| Environment Variable | Default                          | Description                    |
| -------------------- | -------------------------------- | ------------------------------ |
| steps                | 30000                            | Training Iterations            |
| pipeline             | /global/training/pipeline.config | Training Config Pipeline Path  |
| checkpoint           | /global/output/checkpoint        | Save training checkpoint path  |
| frozen               | /global/output/frozen            | Save training frozen mode path |
| openvino             | /global/output/openvino          | Save OpenVINO model path       |

### TensorBoard

Open new terminal, then run and get running container ID 

```
docker ps
```

then, inspect container by running:

```
docker exec -it CONTAINER_ID bash
```

 run tensorboard

```
tensorbard --logdir /global/output/checkpoint/
```

Now open [localhost:5001]() at host browser

### Current Version

v0.1 (06-12-2020) using TensorFlow 1.15, OpenVINO 2020.3, CUDA10.0