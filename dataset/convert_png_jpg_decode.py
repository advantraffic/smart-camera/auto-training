import glob, os
import random
import argparse
from PIL import Image

parser = argparse.ArgumentParser(description='Convert VOC Dataset Images from png to jpg')
parser.add_argument('--dataset', required=True,
                    help='path for dataset folder which contains Annotations and JPEGImages folder.')
					
args = parser.parse_args()

img_folder = os.path.join(args.dataset, "CAM94_Noon")
imgs_png = glob.glob(os.path.join(img_folder, "*.jpg"))

for pathAndFilename in imgs_png:
    img = Image.open(pathAndFilename).convert('RGB')
    img.save(pathAndFilename, 'jpeg')
