import glob, os
import random
import argparse

parser = argparse.ArgumentParser(description='Convert VOC Dataset Images from png to jpg')
parser.add_argument('--dataset', required=True,
                    help='path for dataset folder which contains Annotations and JPEGImages folder.')
					
args = parser.parse_args()

img_folder = os.path.join(args.dataset, "JPEGImages")
imgs_png = glob.glob(os.path.join(img_folder, "*.png"))

for pathAndFilename in imgs_png:  
    title, ext = os.path.splitext(os.path.basename(pathAndFilename))
    dir = os.path.dirname(pathAndFilename)
    new_name = os.path.join(dir, f"{title}.jpg")
    os.rename(pathAndFilename, new_name)
