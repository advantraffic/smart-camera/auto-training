import glob, os
import random
import argparse

parser = argparse.ArgumentParser(description='Generate train.txt and test.txt for VOC Dataset')
parser.add_argument('--dataset', required=True,
                    help='path for dataset folder which contains Annotations and JPEGImages folder.')
					
args = parser.parse_args()

txt_folder = os.path.join(args.dataset, "ImageSets", "Main")
if not os.path.exists(txt_folder):
    os.makedirs(txt_folder)
# Percentage of images to be used for the test set
percentage_test = 10;
# Create and/or truncate train.txt and test.txt
file_train = open(os.path.join(txt_folder, 'train.txt'), 'w')  
file_test = open(os.path.join(txt_folder, 'test.txt'), 'w')
index_test = round(100 / percentage_test)  
datasetXML = glob.glob(os.path.join(args.dataset, "Annotations", "*.xml"))
random.shuffle(datasetXML)
counter = 1  
for pathAndFilename in datasetXML:  
    title, ext = os.path.splitext(os.path.basename(pathAndFilename))
    if counter == index_test:
        counter = 1
        file_test.write(title + "\n")
    else:
        file_train.write(title + "\n")
        counter = counter + 1
